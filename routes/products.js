const { Product, validate } = require('../models/products');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const products = await Product.find().sort('name');
	res.send(products);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let product = new Product({
		// Product:
		id: req.body.id,
		name: req.body.name
	});

	// let email = await Product.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	product = await product.save();

	res.send({ message: 'Product added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const product = await Product.findByIdAndUpdate(
		req.params.id,
		{
			// Product:
			id: req.body.id,
			name: req.body.name
		},
		{ new: true }
	);

	if (!product)
		return res
			.status(404)
			.send('The product with the given ID was not found.');

	res.send({ message: 'Product updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const product = await Product.findByIdAndRemove(req.params.id);

	if (!product)
		return res
			.status(404)
			.send('The product with the given ID was not found.');

	res.send({ message: 'Product deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const product = await Product.findById(req.params.id);

	if (!product)
		return res
			.status(404)
			.send('The product with the given ID was not found.');

	res.send(product);
});

module.exports = router;
