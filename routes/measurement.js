const { Measurement, validate } = require('../models/measurement');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const measurements = await Measurement.find().sort('name');
	res.send(measurements);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let measurement = new Measurement({
		// Top:
		kurti_type: req.body.kurti_type,
		kurti_length: req.body.kurti_length,
		chest_length: req.body.chest_length,
		waist_length: req.body.waist_length,
		heap_length: req.body.heap_length,
		// Neck:
		neck_type: req.body.neck_type,
		front_neck_length: req.body.front_neck_length,
		back_neck_length: req.body.back_neck_length,
		// Sleaves:
		sleaves_type: req.body.sleaves_type,
		sleaves_length: req.body.sleaves_length,
		arm_holes: req.body.arm_holes,
		arm_round_left: req.body.arm_round_left,
		arm_round_right: req.body.arm_round_right,
		sleaves_flare: req.body.sleaves_flare,
		// Chest:
		upper_chest: req.body.upper_chest,
		lower_chest: req.body.lower_chest,
		waist: req.body.waist,
		heap: req.body.heap,
		seat: req.body.seat,
		other: req.body.other,
		// Bottom
		bottom_length: req.body.bottom_length,
		bottom_waist: req.body.bottom_waist,
			bottom_heap: req.body.bottom_heap,
			bottom_seat: req.body.bottom_seat,
			bottom_theigh: req.body.bottom_theigh,
			bottom_knee: req.body.bottom_knee,
			bottom_calf: req.body.bottom_calf,
			bottom_ancle: req.body.bottom_ancle,
			bottom_bottom: req.body.bottom_bottom,
			bottom_other: req.body.bottom_other
	});

	// let email = await Measurement.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	measurement = await measurement.save();

	res.send({ message: 'Measurement added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const measurement = await Measurement.findByIdAndUpdate(
		req.params.id,
		{
			// Top:
			kurti_type: req.body.kurti_type,
			kurti_length: req.body.kurti_length,
			chest_length: req.body.chest_length,
			waist_length: req.body.waist_length,
			heap_length: req.body.heap_length,
			// Neck:
			neck_type: req.body.neck_type,
			front_neck_length: req.body.front_neck_length,
			back_neck_length: req.body.back_neck_length,
			// Sleaves:
			sleaves_type: req.body.sleaves_type,
			sleaves_length: req.body.sleaves_length,
			arm_holes: req.body.arm_holes,
			arm_round_left: req.body.arm_round_left,
			arm_round_right: req.body.arm_round_right,
			sleaves_flare: req.body.sleaves_flare,
			// Chest:
			upper_chest: req.body.upper_chest,
			lower_chest: req.body.lower_chest,
			waist: req.body.waist,
			heap: req.body.heap,
			seat: req.body.seat,
			other: req.body.other,
			// Botttom
			bottom_length: req.body.bottom_length,
			bottom_waist: req.body.bottom_waist,
			bottom_heap: req.body.bottom_heap,
			bottom_seat: req.body.bottom_seat,
			bottom_theigh: req.body.bottom_theigh,
			bottom_knee: req.body.bottom_knee,
			bottom_calf: req.body.bottom_calf,
			bottom_ancle: req.body.bottom_ancle,
			bottom_bottom: req.body.bottom_bottom,
			bottom_other: req.body.bottom_other
		},
		{ new: true }
	);

	if (!measurement)
		return res
			.status(404)
			.send('The measurement with the given ID was not found.');

	res.send({ message: 'Measurement updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const measurement = await Measurement.findByIdAndRemove(req.params.id);

	if (!measurement)
		return res
			.status(404)
			.send('The measurement with the given ID was not found.');

	res.send({ message: 'Measurement deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const measurement = await Measurement.findById(req.params.id);

	if (!measurement)
		return res
			.status(404)
			.send('The measurement with the given ID was not found.');

	res.send(measurement);
});

module.exports = router;
