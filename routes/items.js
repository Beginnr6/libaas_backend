const { Item, validate } = require("../models/items");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const items = await Item.find().sort("name");
  res.send(items);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let item = new Item({
    name: req.body.name,
    category: req.body.category,
    price: req.body.price,
    discount: req.body.discount,
    description: req.body.description,
    created_on: req.body.created_on,
  });

  let name = await Item.findOne({
    name: req.body.name,
  });
  if (name) return res.status(400).send("Item already registered.");

  item = await item.save();

  res.send({ message: "Item added successfully" });
});

router.put("/:id", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const item = await Item.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        category: req.body.category,
        price: req.body.price,
        discount: req.body.discount,
        description: req.body.description,
        created_on: req.body.created_on,
    },
    { new: true }
  );

  if (!item)
    return res.status(404).send("The item with the given ID was not found.");

  res.send({ message: "Item updeted successfully" });
});

router.delete("/:id", async (req, res) => {
  const item = await Item.findByIdAndRemove(req.params.id);

  if (!item)
    return res.status(404).send("The item with the given ID was not found.");

  res.send({ message: "Item deleted successfully" });
});

router.get("/:id", async (req, res) => {
  const item = await Item.findById(req.params.id);

  if (!item)
    return res.status(404).send("The item with the given ID was not found.");

  res.send(item);
});

module.exports = router;
