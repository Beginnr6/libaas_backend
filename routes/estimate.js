const { Estimate, validate } = require("../models/estimate");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const estimates = await Estimate.find().sort("estimate_no");
  res.send(estimates);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let estimate = new Estimate({
    estimate_no: req.body.estimate_no,
    customer: req.body.customer,
    estimate_date: req.body.estimate_date,
    expiry_date: req.body.expiry_date,
    amount: req.body.amount,
    status: req.body.status,
  });

  let estimate_no = await Estimate.findOne({
    estimate_no: req.body.estimate_no,
  });
  if (estimate_no) return res.status(400).send("Estimate No. already registered.");

  estimate = await estimate.save();

  res.send({ message: "Estimate added successfully" });
});

router.put("/:id", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const estimate = await Estimate.findByIdAndUpdate(
    req.params.id,
    {
      estimate_no: req.body.estimate_no,
      customer: req.body.customer,
      estimate_date: req.body.estimate_date,
      expiry_date: req.body.expiry_date,
      amount: req.body.amount,
      status: req.body.status,
    },
    { new: true }
  );

  if (!estimate)
    return res.status(404).send("The estimate with the given ID was not found.");

  res.send({ message: "Estimate updeted successfully" });
});

router.delete("/:id", async (req, res) => {
  const estimate = await Estimate.findByIdAndRemove(req.params.id);

  if (!estimate)
    return res.status(404).send("The estimate with the given ID was not found.");

  res.send({ message: "Estimate deleted successfully" });
});

router.get("/:id", async (req, res) => {
  const estimate = await Estimate.findById(req.params.id);

  if (!estimate)
    return res.status(404).send("The estimate with the given ID was not found.");

  res.send(estimate);
});

module.exports = router;
