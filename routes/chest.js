const { Chest, validate } = require('../models/chest');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const chests = await Chest.find().sort('name');
	res.send(chests);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let chest = new Chest({
		// Chest:
		upper_chest: req.body.upper_chest,
		lower_chest: req.body.lower_chest,
		waist: req.body.waist,
		heap: req.body.heap,
		seat: req.body.seat,
		other: req.body.other,
	});

	// let email = await Chest.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	chest = await chest.save();

	res.send({ message: 'Chest added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const chest = await Chest.findByIdAndUpdate(
		req.params.id,
		{
			// Chest:
			upper_chest: req.body.upper_chest,
			lower_chest: req.body.lower_chest,
			waist: req.body.waist,
			heap: req.body.heap,
			seat: req.body.seat,
			other: req.body.other,
		},
		{ new: true }
	);

	if (!chest)
		return res
			.status(404)
			.send('The chest with the given ID was not found.');

	res.send({ message: 'Chest updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const chest = await Chest.findByIdAndRemove(req.params.id);

	if (!chest)
		return res
			.status(404)
			.send('The chest with the given ID was not found.');

	res.send({ message: 'Chest deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const chest = await Chest.findById(req.params.id);

	if (!chest)
		return res
			.status(404)
			.send('The chest with the given ID was not found.');

	res.send(chest);
});

module.exports = router;
