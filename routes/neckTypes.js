const { NeckType, validate } = require('../models/neckTypes');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const neckTypes = await NeckType.find().sort('name');
	res.send(neckTypes);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let neckType = new NeckType({
		// NeckType:
		id: req.body.id,
		name: req.body.name
	});

	// let email = await NeckType.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	neckType = await neckType.save();

	res.send({ message: 'NeckType added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const neckType = await NeckType.findByIdAndUpdate(
		req.params.id,
		{
			// NeckType:
			id: req.body.id,
			name: req.body.name
		},
		{ new: true }
	);

	if (!neckType)
		return res
			.status(404)
			.send('The neckType with the given ID was not found.');

	res.send({ message: 'NeckType updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const neckType = await NeckType.findByIdAndRemove(req.params.id);

	if (!neckType)
		return res
			.status(404)
			.send('The neckType with the given ID was not found.');

	res.send({ message: 'NeckType deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const neckType = await NeckType.findById(req.params.id);

	if (!neckType)
		return res
			.status(404)
			.send('The neckType with the given ID was not found.');

	res.send(neckType);
});

module.exports = router;
