const { TopType, validate } = require('../models/topTypes');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const topTypes = await TopType.find().sort('name');
	res.send(topTypes);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let topType = new TopType({
		// TopType:
		id: req.body.id,
		name: req.body.name
	});

	// let email = await TopType.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	topType = await topType.save();

	res.send({ message: 'TopType added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const topType = await TopType.findByIdAndUpdate(
		req.params.id,
		{
			// TopType:
			id: req.body.id,
			name: req.body.name
		},
		{ new: true }
	);

	if (!topType)
		return res
			.status(404)
			.send('The topType with the given ID was not found.');

	res.send({ message: 'TopType updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const topType = await TopType.findByIdAndRemove(req.params.id);

	if (!topType)
		return res
			.status(404)
			.send('The topType with the given ID was not found.');

	res.send({ message: 'TopType deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const topType = await TopType.findById(req.params.id);

	if (!topType)
		return res
			.status(404)
			.send('The topType with the given ID was not found.');

	res.send(topType);
});

module.exports = router;
