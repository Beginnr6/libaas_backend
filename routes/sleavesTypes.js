const { SleavesType, validate } = require('../models/sleavesTypes');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const sleavesTypes = await SleavesType.find().sort('name');
	res.send(sleavesTypes);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let sleavesType = new SleavesType({
		// SleavesType:
		id: req.body.id,
		name: req.body.name
	});

	// let email = await SleavesType.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	sleavesType = await sleavesType.save();

	res.send({ message: 'SleavesType added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const sleavesType = await SleavesType.findByIdAndUpdate(
		req.params.id,
		{
			// SleavesType:
			id: req.body.id,
			name: req.body.name
		},
		{ new: true }
	);

	if (!sleavesType)
		return res
			.status(404)
			.send('The sleavesType with the given ID was not found.');

	res.send({ message: 'SleavesType updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const sleavesType = await SleavesType.findByIdAndRemove(req.params.id);

	if (!sleavesType)
		return res
			.status(404)
			.send('The sleavesType with the given ID was not found.');

	res.send({ message: 'SleavesType deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const sleavesType = await SleavesType.findById(req.params.id);

	if (!sleavesType)
		return res
			.status(404)
			.send('The sleavesType with the given ID was not found.');

	res.send(sleavesType);
});

module.exports = router;
