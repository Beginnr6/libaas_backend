const { BottomType, validate } = require('../models/bottomTypes');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const bottomTypes = await BottomType.find().sort('name');
	res.send(bottomTypes);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let bottomType = new BottomType({
		// BottomType:
		id: req.body.id,
		name: req.body.name
	});

	// let email = await BottomType.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	bottomType = await bottomType.save();

	res.send({ message: 'BottomType added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const bottomType = await BottomType.findByIdAndUpdate(
		req.params.id,
		{
			// BottomType:
			id: req.body.id,
			name: req.body.name
		},
		{ new: true }
	);

	if (!bottomType)
		return res
			.status(404)
			.send('The bottomType with the given ID was not found.');

	res.send({ message: 'BottomType updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const bottomType = await BottomType.findByIdAndRemove(req.params.id);

	if (!bottomType)
		return res
			.status(404)
			.send('The bottomType with the given ID was not found.');

	res.send({ message: 'BottomType deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const bottomType = await BottomType.findById(req.params.id);

	if (!bottomType)
		return res
			.status(404)
			.send('The bottomType with the given ID was not found.');

	res.send(bottomType);
});

module.exports = router;
