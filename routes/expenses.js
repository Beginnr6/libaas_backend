const { Expense, validate } = require("../models/expenses");
// const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const expenses = await Expense.find().sort("expense_date");
  res.send(expenses);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let expense = new Expense({
    category: req.body.category,
    customer: req.body.customer,
    expense_date: req.body.expense_date,
    notes: req.body.notes,
    amount: req.body.amount,
    status: req.body.status,
  });

  expense = await expense.save();

  res.send({ message: "Expense added successfully" });
});

router.put("/:id", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const expense = await Expense.findByIdAndUpdate(
    req.params.id,
    {
      category: req.body.category,
      customer: req.body.customer,
      expense_date: req.body.expense_date,
      notes: req.body.notes,
      amount: req.body.amount,
      status: req.body.status,
    },
    { new: true }
  );

  if (!expense)
    return res.status(404).send("The expense with the given ID was not found.");

  res.send({ message: "Expense updeted successfully" });
});

router.delete("/:id", async (req, res) => {
  const expense = await Expense.findByIdAndRemove(req.params.id);

  if (!expense)
    return res.status(404).send("The expense with the given ID was not found.");

  res.send({ message: "Expense deleted successfully" });
});

router.get("/:id", async (req, res) => {
  const expense = await Expense.findById(req.params.id);

  if (!expense)
    return res.status(404).send("The expense with the given ID was not found.");

  res.send(expense);
});

module.exports = router;
