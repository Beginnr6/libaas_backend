const { Neck, validate } = require('../models/neck');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const necks = await Neck.find().sort('name');
	res.send(necks);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let neck = new Neck({
		// Neck:
		neck_type: req.body.neck_type,
		front_neck_length: req.body.front_neck_length,
		back_neck_length: req.body.back_neck_length,
	});

	// let email = await Neck.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	neck = await neck.save();

	res.send({ message: 'Neck added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const neck = await Neck.findByIdAndUpdate(
		req.params.id,
		{
			// Neck:
			neck_type: req.body.neck_type,
			front_neck_length: req.body.front_neck_length,
			back_neck_length: req.body.back_neck_length,
		},
		{ new: true }
	);

	if (!neck)
		return res
			.status(404)
			.send('The neck with the given ID was not found.');

	res.send({ message: 'Neck updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const neck = await Neck.findByIdAndRemove(req.params.id);

	if (!neck)
		return res
			.status(404)
			.send('The neck with the given ID was not found.');

	res.send({ message: 'Neck deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const neck = await Neck.findById(req.params.id);

	if (!neck)
		return res
			.status(404)
			.send('The neck with the given ID was not found.');

	res.send(neck);
});

module.exports = router;
