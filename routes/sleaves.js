const { Sleaves, validate } = require('../models/sleaves');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const sleaves = await Sleaves.find().sort('name');
	res.send(sleaves);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let sleaves = new Sleaves({
		// Sleaves:
		sleaves_type: req.body.sleaves_type,
		sleaves_length: req.body.sleaves_length,
		arm_holes: req.body.arm_holes,
		arm_round_left: req.body.arm_round_left,
		arm_round_right: req.body.arm_round_right,
		sleaves_flare: req.body.sleaves_flare,

	});

	// let email = await Sleaves.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	sleaves = await sleaves.save();

	res.send({ message: 'Sleaves added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const sleaves = await Sleaves.findByIdAndUpdate(
		req.params.id,
		{
			// Sleaves:
			sleaves_type: req.body.sleaves_type,
			sleaves_length: req.body.sleaves_length,
			arm_holes: req.body.arm_holes,
			arm_round_left: req.body.arm_round_left,
			arm_round_right: req.body.arm_round_right,
			sleaves_flare: req.body.sleaves_flare,
		},
		{ new: true }
	);

	if (!sleaves)
		return res
			.status(404)
			.send('The sleaves with the given ID was not found.');

	res.send({ message: 'Sleaves updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const sleaves = await Sleaves.findByIdAndRemove(req.params.id);

	if (!sleaves)
		return res
			.status(404)
			.send('The sleaves with the given ID was not found.');

	res.send({ message: 'Sleaves deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const sleaves = await Sleaves.findById(req.params.id);

	if (!sleaves)
		return res
			.status(404)
			.send('The sleaves with the given ID was not found.');

	res.send(sleaves);
});

module.exports = router;
