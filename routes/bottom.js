const { Bottom, validate } = require('../models/bottom');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const bottoms = await Bottom.find().sort('name');
	res.send(bottoms);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let bottom = new Bottom({
		// Bottom
		bottom_type: req.body.bottom_type,
		bottom_length: req.body.bottom_length,
		bottom_waist: req.body.bottom_waist,
			bottom_heap: req.body.bottom_heap,
			bottom_seat: req.body.bottom_seat,
			bottom_theigh: req.body.bottom_theigh,
			bottom_knee: req.body.bottom_knee,
			bottom_calf: req.body.bottom_calf,
			bottom_ancle: req.body.bottom_ancle,
			bottom_bottom: req.body.bottom_bottom,
			bottom_other: req.body.bottom_other
	});

	// let email = await Bottom.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	bottom = await bottom.save();

	res.send({ message: 'Bottom added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const bottom = await Bottom.findByIdAndUpdate(
		req.params.id,
		{
			// Botttom
			bottom_type: req.body.bottom_type,
			bottom_length: req.body.bottom_length,
			bottom_waist: req.body.bottom_waist,
			bottom_heap: req.body.bottom_heap,
			bottom_seat: req.body.bottom_seat,
			bottom_theigh: req.body.bottom_theigh,
			bottom_knee: req.body.bottom_knee,
			bottom_calf: req.body.bottom_calf,
			bottom_ancle: req.body.bottom_ancle,
			bottom_bottom: req.body.bottom_bottom,
			bottom_other: req.body.bottom_other
		},
		{ new: true }
	);

	if (!bottom)
		return res
			.status(404)
			.send('The bottom with the given ID was not found.');

	res.send({ message: 'Bottom updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const bottom = await Bottom.findByIdAndRemove(req.params.id);

	if (!bottom)
		return res
			.status(404)
			.send('The bottom with the given ID was not found.');

	res.send({ message: 'Bottom deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const bottom = await Bottom.findById(req.params.id);

	if (!bottom)
		return res
			.status(404)
			.send('The bottom with the given ID was not found.');

	res.send(bottom);
});

module.exports = router;
