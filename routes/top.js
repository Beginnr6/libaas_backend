const { Top, validate } = require('../models/top');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
	const tops = await Top.find().sort('name');
	res.send(tops);
});

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let top = new Top({
		// Top:
		kurti_type: req.body.kurti_type,
		kurti_length: req.body.kurti_length,
		chest_length: req.body.chest_length,
		waist_length: req.body.waist_length,
		heap_length: req.body.heap_length
	});

	// let email = await Top.findOne({ email: req.body.email });
	// if (email) return res.status(400).send('User already registered.');

	top = await top.save();

	res.send({ message: 'Top added successfully' });
});

router.put('/:id', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	const top = await Top.findByIdAndUpdate(
		req.params.id,
		{
			// Top:
			kurti_type: req.body.kurti_type,
			kurti_length: req.body.kurti_length,
			chest_length: req.body.chest_length,
			waist_length: req.body.waist_length,
			heap_length: req.body.heap_length
		},
		{ new: true }
	);

	if (!top)
		return res
			.status(404)
			.send('The top with the given ID was not found.');

	res.send({ message: 'Top updeted successfully' });
});

router.delete('/:id', auth, async (req, res) => {
	const top = await Top.findByIdAndRemove(req.params.id);

	if (!top)
		return res
			.status(404)
			.send('The top with the given ID was not found.');

	res.send({ message: 'Top deleted successfully' });
});

router.get('/:id', async (req, res) => {
	const top = await Top.findById(req.params.id);

	if (!top)
		return res
			.status(404)
			.send('The top with the given ID was not found.');

	res.send(top);
});

module.exports = router;
