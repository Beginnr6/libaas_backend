const {Category, validate} = require('../models/categories');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
  const categories = await Category.find().sort('created_on');
  res.send(categories);
});

router.post('/', async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let category = new Category({
    category_name: req.body.category_name,
    description: req.body.description,
    created_on: req.body.created_on,
  });

  let category_name = await Category.findOne({ category_name: req.body.category_name });
  if (category_name) return res.status(400).send('Category already registered.');

  category = await category.save();

  res.send({message:'Category added successfully'});
});

router.put('/:id', async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const category = await Category.findByIdAndUpdate(req.params.id,
    {
        category_name: req.body.category_name,
        description: req.body.description,
        created_on: req.body.created_on,
      }, { new: true });

  if (!category) return res.status(404).send('The category with the given ID was not found.');

  res.send({message:'Category updeted successfully'});
});

router.delete('/:id', async (req, res) => {
  const category = await Category.findByIdAndRemove(req.params.id);

  if (!category) return res.status(404).send('The category with the given ID was not found.');

  res.send({message:'Category deleted successfully'});
});

router.get('/:id', async (req, res) => {
  const category = await Category.findById(req.params.id);

  if (!category) return res.status(404).send('The category with the given ID was not found.');

  res.send(category);
});

module.exports = router;