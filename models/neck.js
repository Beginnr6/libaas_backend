const Joi = require('joi');
const mongoose = require('mongoose');

const Neck = mongoose.model(
	'Neck',
	new mongoose.Schema({
		//   Neck:
		neck_type: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		front_neck_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		back_neck_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},

	})
);

function validateNeck(neck) {
	const schema = {
		// Neck:
		neck_type: Joi.string().min(1).max(50).required(),
		front_neck_length: Joi.number().min(1).max(50).required(),
		back_neck_length: Joi.number().min(1).max(50).required()
	};
	return Joi.validate(neck, schema);
}

exports.Neck = Neck;
exports.validate = validateNeck;
