const Joi = require('joi');
const mongoose = require('mongoose');

const Top = mongoose.model(
	'Top',
	new mongoose.Schema({
		// Top:
		kurti_type: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		kurti_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 10
		},
		chest_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		waist_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		heap_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		}
	})
);

function validateTop(top) {
	const schema = {
		// Top:
		kurti_type: Joi.string().min(1).max(50).required(),
		kurti_length: Joi.number().min(1).max(50).required(),
		chest_length: Joi.number().min(1).max(50).required(),
		waist_length: Joi.number().min(1).max(50).required(),
		heap_length: Joi.number().min(1).max(50).required()
	};
	return Joi.validate(top, schema);
}

exports.Top = Top;
exports.validate = validateTop;
