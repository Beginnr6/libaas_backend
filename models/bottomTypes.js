const Joi = require('joi');
const number = require('joi/lib/types/number');
const mongoose = require('mongoose');

const BottomType = mongoose.model(
	'BottomType',
	new mongoose.Schema({
		// BottomType:
		id: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		name: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 10
		}
	})
);

function validateBottomType(bottomType) {
	const schema = {
		// BottomType:
		id: Joi.number().min(1).max(50).required(),
		name: Joi.string().min(1).max(50).required(),
	};
	return Joi.validate(bottomType, schema);
}

exports.BottomType = BottomType;
exports.validate = validateBottomType;
