const Joi = require('joi');
const mongoose = require('mongoose');

const Measurement = mongoose.model(
	'Measurement',
	new mongoose.Schema({
		// Top:
		kurti_type: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		kurti_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 10
		},
		chest_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		waist_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		heap_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		//   Neck:
		neck_type: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		front_neck_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		back_neck_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		//   Sleaves:
		sleaves_type: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		sleaves_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		arm_holes: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		arm_round_left: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		arm_round_right: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		sleaves_flare: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		//   Chest:
		upper_chest: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		lower_chest: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		waist: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		heap: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		seat: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		heap: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		other: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		// Bottom
		bottom_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_waist: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_heap: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_seat: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_theigh: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_knee: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_calf: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_ancle: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_bottom: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_other: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		}
	})
);

function validateMeasurement(measurement) {
	const schema = {
		// Top:
		kurti_type: Joi.string().min(1).max(50).required(),
		kurti_length: Joi.number().min(1).max(50).required(),
		chest_length: Joi.number().min(1).max(50).required(),
		waist_length: Joi.number().min(1).max(50).required(),
		heap_length: Joi.number().min(1).max(50).required(),
		// Neck:
		neck_type: Joi.string().min(1).max(50).required(),
		front_neck_length: Joi.number().min(1).max(50).required(),
		back_neck_length: Joi.number().min(1).max(50).required(),
		// Sleaves:
		sleaves_type: Joi.string().min(1).max(50).required(),
		sleaves_length: Joi.number().min(1).max(50).required(),
		arm_holes: Joi.number().min(1).max(50).required(),
		arm_round_left: Joi.number().min(1).max(50).required(),
		arm_round_right: Joi.number().min(1).max(50).required(),
		sleaves_flare: Joi.number().min(1).max(50).required(),
		// Chest:
		upper_chest: Joi.number().min(1).max(50).required(),
		lower_chest: Joi.number().min(1).max(50).required(),
		waist: Joi.number().min(1).max(50).required(),
		heap: Joi.number().min(1).max(50).required(),
		seat: Joi.number().min(1).max(50).required(),
		other: Joi.number().min(1).max(50).required(),
		// Bottom:
		bottom_length: Joi.number().min(1).max(50).required(),
		bottom_waist: Joi.number().min(1).max(50).required(),
		bottom_heap: Joi.number().min(1).max(50).required(),
		bottom_seat: Joi.number().min(1).max(50).required(),
		bottom_theigh: Joi.number().min(1).max(50).required(),
		bottom_knee: Joi.number().min(1).max(50).required(),
		bottom_calf: Joi.number().min(1).max(50).required(),
		bottom_ancle: Joi.number().min(1).max(50).required(),
		bottom_bottom: Joi.number().min(1).max(50).required(),
		bottom_other: Joi.number().min(1).max(50).required(),

	};
	return Joi.validate(measurement, schema);
}

exports.Measurement = Measurement;
exports.validate = validateMeasurement;
