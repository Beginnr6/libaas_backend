const Joi = require('joi');
const date = require('joi/lib/types/date');
const mongoose = require('mongoose');

const Item = mongoose.model('Item', new mongoose.Schema({
    name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  category: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  price: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 10
  },
  discount: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  created_on: {
    type: String,
    required: true,
  }
}));

function validateItem(Item) {
  const schema = {
    name: Joi.string().min(5).max(50).required(),
    category: Joi.string().min(5).max(50).required(),
    price: Joi.string().min(2).max(10).required(),
    discount: Joi.string().required(),
    description: Joi.string().min(5).max(50).required(),
    created_on: Joi.string().required(),
  };

  return Joi.validate(Item, schema);
}

exports.Item = Item;
exports.validate = validateItem;