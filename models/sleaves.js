const Joi = require('joi');
const mongoose = require('mongoose');

const Sleaves = mongoose.model(
	'Sleaves',
	new mongoose.Schema({

		//   Sleaves:
		sleaves_type: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		sleaves_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		arm_holes: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		arm_round_left: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		arm_round_right: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		sleaves_flare: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},

	})
);

function validateSleaves(sleaves) {
	const schema = {
		// Sleaves:
		sleaves_type: Joi.string().min(1).max(50).required(),
		sleaves_length: Joi.number().min(1).max(50).required(),
		arm_holes: Joi.number().min(1).max(50).required(),
		arm_round_left: Joi.number().min(1).max(50).required(),
		arm_round_right: Joi.number().min(1).max(50).required(),
		sleaves_flare: Joi.number().min(1).max(50).required(),

	};
	return Joi.validate(sleaves, schema);
}

exports.Sleaves = Sleaves;
exports.validate = validateSleaves;
