const Joi = require('joi');
const number = require('joi/lib/types/number');
const mongoose = require('mongoose');

const NeckType = mongoose.model(
	'NeckType',
	new mongoose.Schema({
		// NeckType:
		id: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		name: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 10
		}
	})
);

function validateNeckType(neckType) {
	const schema = {
		// NeckType:
		id: Joi.number().min(1).max(50).required(),
		name: Joi.string().min(1).max(50).required(),
	};
	return Joi.validate(neckType, schema);
}

exports.NeckType = NeckType;
exports.validate = validateNeckType;
