const Joi = require('joi');
const mongoose = require('mongoose');

const Chest = mongoose.model(
	'Chest',
	new mongoose.Schema({
		//   Chest:
		upper_chest: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		lower_chest: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		waist: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		heap: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		seat: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		heap: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		other: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},

	})
);

function validateChest(chest) {
	const schema = {
		// Chest:
		upper_chest: Joi.number().min(1).max(50).required(),
		lower_chest: Joi.number().min(1).max(50).required(),
		waist: Joi.number().min(1).max(50).required(),
		heap: Joi.number().min(1).max(50).required(),
		seat: Joi.number().min(1).max(50).required(),
		other: Joi.number().min(1).max(50).required(),

	};
	return Joi.validate(chest, schema);
}

exports.Chest = Chest;
exports.validate = validateChest;
