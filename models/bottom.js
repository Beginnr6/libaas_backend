const Joi = require('joi');
const mongoose = require('mongoose');

const Bottom = mongoose.model(
	'Bottom',
	new mongoose.Schema({
		// Bottom
		bottom_type: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_length: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_waist: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_heap: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_seat: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_theigh: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_knee: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_calf: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_ancle: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_bottom: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		bottom_other: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		}
	})
);

function validateBottom(bottom) {
	const schema = {
		// Bottom:
		bottom_type: Joi.number().min(1).max(50).required(),
		bottom_length: Joi.number().min(1).max(50).required(),
		bottom_waist: Joi.number().min(1).max(50).required(),
		bottom_heap: Joi.number().min(1).max(50).required(),
		bottom_seat: Joi.number().min(1).max(50).required(),
		bottom_theigh: Joi.number().min(1).max(50).required(),
		bottom_knee: Joi.number().min(1).max(50).required(),
		bottom_calf: Joi.number().min(1).max(50).required(),
		bottom_ancle: Joi.number().min(1).max(50).required(),
		bottom_bottom: Joi.number().min(1).max(50).required(),
		bottom_other: Joi.number().min(1).max(50).required(),

	};
	return Joi.validate(bottom, schema);
}

exports.Bottom = Bottom;
exports.validate = validateBottom;
