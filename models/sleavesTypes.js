const Joi = require('joi');
const number = require('joi/lib/types/number');
const mongoose = require('mongoose');

const SleavesType = mongoose.model(
	'SleavesType',
	new mongoose.Schema({
		// SleavesType:
		id: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		name: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 10
		}
	})
);

function validateSleavesType(sleavesType) {
	const schema = {
		// SleavesType:
		id: Joi.number().min(1).max(50).required(),
		name: Joi.string().min(1).max(50).required(),
	};
	return Joi.validate(sleavesType, schema);
}

exports.SleavesType = SleavesType;
exports.validate = validateSleavesType;
