const Joi = require("joi");
const mongoose = require("mongoose");

const Category = mongoose.model(
  "Category",
  new mongoose.Schema({
    category_name: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 50,
    },
    description: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 50,
    },
    created_on: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 50,
    },
  })
);

function validateCategory(customer) {
  const schema = {
    category_name: Joi.string().min(5).max(50).required(),
    description: Joi.string().min(5).max(50).required(),
    created_on: Joi.string().min(5).max(50).required(),
  };

  return Joi.validate(customer, schema);
}

exports.Category = Category;
exports.validate = validateCategory;
