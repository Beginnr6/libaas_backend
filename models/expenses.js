const Joi = require('joi');
const mongoose = require('mongoose');

const Expense = mongoose.model('Expense', new mongoose.Schema({
  category: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  customer: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  expense_date: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 50
  },
  notes: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 50
  },
  amount: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 50
  },
  status: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  }
}));

function validateExpense(expense) {
  const schema = {
    category: Joi.string().min(5).max(50).required(),
    customer: Joi.string().min(5).max(50).required(),
    expense_date: Joi.string().min(6).max(20).required(),
    notes: Joi.string().min(6).max(50).required(),
    amount: Joi.string().min(5).max(50).required(),
    status: Joi.string().min(5).max(50).required(),
  };

  return Joi.validate(expense, schema);
}

exports.Expense = Expense;
exports.validate = validateExpense;