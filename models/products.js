const Joi = require('joi');
const number = require('joi/lib/types/number');
const mongoose = require('mongoose');

const Product = mongoose.model(
	'Product',
	new mongoose.Schema({
		// Product:
		id: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		name: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 10
		}
	})
);

function validateProduct(product) {
	const schema = {
		// Product:
		id: Joi.number().min(1).max(50).required(),
		name: Joi.string().min(1).max(50).required(),
	};
	return Joi.validate(product, schema);
}

exports.Product = Product;
exports.validate = validateProduct;
