const Joi = require('joi');
const mongoose = require('mongoose');

const Customer = mongoose.model('Customer', new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  gender: {
    type: String,
    required: true,
    minlength: 4,
    maxlength: 10
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  city: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  state: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  country: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  // address: {
  //   type: String,
  //   required: true,
  //   minlength: 5,
  //   maxlength: 50
  // },
  zip: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  phone: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
//   avatar: {
//     type: Buffer, // casted to MongoDB's BSON type: binData
//     required: true
// }
}));

function validateCustomer(customer) {
  const schema = {
    name: Joi.string().min(5).max(50).required(),
    gender: Joi.string().min(4).max(10).required(),
    email: Joi.string().min(5).max(50).required(),
    city: Joi.string().min(5).max(50).required(),
    state: Joi.string().min(5).max(50).required(),
    country: Joi.string().min(5).max(50).required(),
    // address: Joi.string().min(5).max(50).required(),
    zip: Joi.string().min(5).max(50).required(),
    phone: Joi.string().min(5).max(50),
    // phone: Joi.string().Data().Buffer,
  };

  return Joi.validate(customer, schema);
}

exports.Customer = Customer;
exports.validate = validateCustomer;