const Joi = require('joi');
const number = require('joi/lib/types/number');
const mongoose = require('mongoose');

const TopType = mongoose.model(
	'TopType',
	new mongoose.Schema({
		// TopType:
		id: {
			type: Number,
			required: true,
			minlength: 1,
			maxlength: 50
		},
		name: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 10
		}
	})
);

function validateTopType(topType) {
	const schema = {
		// TopType:
		id: Joi.number().min(1).max(50).required(),
		name: Joi.string().min(1).max(50).required(),
	};
	return Joi.validate(topType, schema);
}

exports.TopType = TopType;
exports.validate = validateTopType;
