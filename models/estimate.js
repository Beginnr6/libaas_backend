const Joi = require('joi');
const mongoose = require('mongoose');

const Estimate = mongoose.model('Estimate', new mongoose.Schema({
estimate_no: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  customer: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  estimate_date: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 50
  },
  expiry_date: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 50
  },
  amount: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  status: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  }
}));

function validateEstimate(estimate) {
  const schema = {
    estimate_no: Joi.string().min(5).max(50).required(),
    customer: Joi.string().min(5).max(50).required(),
    estimate_date: Joi.string().min(6).max(20).required(),
    expiry_date: Joi.string().min(6).max(50).required(),
    amount: Joi.string().min(5).max(50).required(),
    status: Joi.string().min(5).max(50).required(),
  };

  return Joi.validate(estimate, schema);
}

exports.Estimate = Estimate;
exports.validate = validateEstimate;